import { CourseService } from './course.service';
import { Component } from '@angular/core';

@Component({
    selector: 'courses',
    template: `
        <h2>{{ title }}</h2>
            <ul>
                <li *ngFor="let course of courses">
                    {{course}}
                </li>
            </ul>
            
            <br/>
            <div (click)="onDivClicked()">
            <button class="btn" (click)="onSave($event)" >Test</button>
            </div>

            <!-- #email is a template variable  -->
            
            <input [(ngModel)]="email" (keyup.enter)="onKeyUp()"/>
            <br/>
            <br/>
            {{ course.title | uppercase}} <br/>
            {{ course.students }} <br/>
            {{ course.rating | number }} <br/>
            {{ course.price }} <br/>
            {{ course.releaseDate }} <br/>

        `
})
export class CoursesComponent{

    email = "me@email.com";

    title = "List of Courses";
    courses;
    imageUrl = "http://lorempixel.com/400/200";
    colSpan=2;
    isActive=false;

    course = {
        title: "The Angular Course",
        rating: 4.975,
        students: 30123,
        price: 190.95,
        releaseDate: new Date(2018, 3, 1)
    }

    onDivClicked(){
        console.log("Div was Clicked");
    }
    onSave($event){
        $event.stopPropagation();
        console.log("Button was clicked", $event);
    }
    onKeyUp(){
        
            console.log(this.email);
    } 

    constructor(service: CourseService){
        
        this.courses = service.getCourses();

    }
}