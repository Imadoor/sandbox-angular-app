import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {

  @Input('isFavorite') isSelected: boolean;

  @Output() change = new EventEmitter();
  courses;

  loadCourses(){
    this.courses = [
      { id:1 , name: 'course1'},
      { id:2 , name: 'course2'},
      { id:3 , name: 'course3'}
    ]
  }
  trackCourse(index, course){
    return course ? course.id : undefined;
  }
  constructor() { }

  ngOnInit() {
  }

  onClick(){
    this.isSelected = !this.isSelected;
    
    this.change.emit({newValue: this.isSelected});
  }


}
