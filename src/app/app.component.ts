import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  canSave= false;
  post={
    title: "Title",
    isFavorite: true
  }

  task = {
    title: 'Review Applications',
    assignee: {
      name: 'John Smith'
    }
  }
  
  onFavoriteChanged(eventArgs){
    console.log("Favorite Changed", eventArgs)
  
  }
  

}
